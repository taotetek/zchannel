#include <czmq.h>
#include "../include/zchannel.h"



void *sender(void *arg) {
    zchanneler_t *chan = (zchanneler_t *) arg;

    int i;
    for (i=0; i<100000; i++) {
        zframe_t *msg = zframe_from ("hello");
        zchanneler_send (chan, msg, 0);
    }

    return NULL;
}

int main(int argc, char *argv [])
{

    int numthreads = atoi(argv[1]);

    pthread_t tid[numthreads];
    
    zchanneler_t *chan = zchanneler_new ("inproc://test");
    assert (chan);

    int i;
    for (i=0; i<numthreads; i++) {
        pthread_create (&(tid[0]), NULL, &sender, chan);
    }
 
    int total = numthreads * 100000;
    for (i=0; i < total; i++) {
        zframe_t *frame = zchanneler_recv (chan);
        char *msg = zframe_strdup (frame);
        assert (!strcmp (msg, "hello"));
        zframe_destroy (&frame);
    }

    zchanneler_destroy (&chan);
}

