/*  =========================================================================
    zchanneler - 
    Thread safe golang like channels with ZeroMQ
        

    Copyright (c) the Contributors as noted in the AUTHORS file.       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    zchanneler - 
    Thread safe golang like channels with ZeroMQ
        
@discuss
@end
*/

#include "../include/zchannel.h"

//  Structure of our class

struct _zchanneler_t {
    zsock_t *server;
    zsock_t *client;
};


//  --------------------------------------------------------------------------
//  Create a new zchanneler.

zchanneler_t *
zchanneler_new (const char *endpoint)
{
    zchanneler_t *self = (zchanneler_t *) zmalloc (sizeof (zchanneler_t));
    assert (self);

    self->server = zsock_new_server (endpoint);
    assert (self->server);

    self->client = zsock_new_client (endpoint);
    assert (self->client);

    return self;
}

//  --------------------------------------------------------------------------
//  Destroys the zchanneler instance.

void
zchanneler_destroy (zchanneler_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        zchanneler_t *self = *self_p;
        zsock_destroy (&self->client);
        zsock_destroy (&self->server);
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Send a frame through the channeler, destroy frame after sending
//  unless ZFRAME_REUSE is set or the attempt to send the message  
//  errors out. Returns -1 on error, 0 on success.                 

int
zchanneler_send (zchanneler_t *self, zframe_t *frame, int flags)
{
    int rc = zframe_send (&frame, self->client, flags);
    return rc;
}

//  --------------------------------------------------------------------------
//  Receive frame from socket, returns zframe_t object or NULL if
//  the recv was interrupted. Does a blocking recv.              

zframe_t *
zchanneler_recv (zchanneler_t *self)
{
    assert (self);
    zframe_t *frame = zframe_recv (self->server);
    return frame;
}

//  --------------------------------------------------------------------------
//  Print properties of the zchanneler object.

void
zchanneler_print (zchanneler_t *self)
{
    assert (self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
zchanneler_test (bool verbose)
{
    printf (" * zchanneler: ");

    //  @selftest
    //  Simple create/destroy test
    zchanneler_t *self = zchanneler_new ("inproc://test");
    assert (self);

    zframe_t *sent = zframe_new ("hello", 5);
    int rc = zchanneler_send (self, sent, 0);
    assert (rc == 0);
    
    zframe_t *received = zchanneler_recv(self);
    char *got = zframe_strdup (received);
    assert (!strcmp(got, "hello"));
    zframe_destroy (&received);
    
    zchanneler_destroy (&self);
    //  @end

    printf ("OK\n");
}
